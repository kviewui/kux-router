import { defineConfig } from 'vite';
import uni from "@dcloudio/vite-plugin-uni";
import generateUniext from './uni_modules/kux-router/vite/vite-plugin-generate-uniext'
import autopages from './uni_modules/kux-autopages'
import kuxgen from './uni_modules/kux-router/vite/vite-plugin-kux-gen';

export default defineConfig({
	plugins: [
		generateUniext,
		uni(),
		autopages({
			pagesJsonMappingFile: 'router2/pages.uts',
		}),
		kuxgen({
			pagesJsonMappingFile: 'router2/pages.uts',
			routerFile: 'router2/router.uts'
		})
	],
})