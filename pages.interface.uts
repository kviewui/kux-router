/**
* pages.json映射文件，该文件每次重新编译时都会由插件自动生成，不要直接在该文件内编辑！！！
* @author kux <kviewui@163.com>
* @created 2024-09-13 20:40:09
* @version 1.0.0
* @copyright Copyright (c) 2024 The Authors.
* @license MIT License
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

export type PagePath = '/pages/index/index' | '/pages/p1/p1' | '/pages/p2/p2' | '/pages/demo/pageCompInterceptor/pageCompInterceptor' | '/pages/p1/login' | '/pages/demo/tabbarInterceptor/tabbarInterceptor' | '/pages/demo/tabbarInterceptor/login' | '/pages/demo/indexInterceptor/indexInterceptor' | '/pages/demo/indexInterceptor/login' | '/pages/demo/routeInfo/routeInfo' | '/pages/p2/login' | '/pages/test1/test1' | '/pages/test2/test2' | '/pages/profile/profile' | '/pages/demo/syncModalInterceptor/syncModalInterceptor' | '/pages/demo/syncModalInterceptor/login' | '/pages/tabint/tabint' | '/pages/demo/query/query';

export type PageName = 'PagesIndexIndex' | 'PagesP1P1' | 'p2' | 'PagesDemoPageCompInterceptorPageCompInterceptor' | 'PagesP1Login' | 'PagesDemoTabbarInterceptorTabbarInterceptor' | 'PagesDemoTabbarInterceptorLogin' | 'PagesDemoIndexInterceptorIndexInterceptor' | 'PagesDemoIndexInterceptorLogin' | 'PagesDemoRouteInfoRouteInfo' | 'PagesP2Login' | 'PagesTest1Test1' | 'PagesTest2Test2' | 'PagesProfileProfile' | 'PagesDemoSyncModalInterceptorSyncModalInterceptor' | 'PagesDemoSyncModalInterceptorLogin' | 'PagesTabintTabint' | 'PagesDemoQueryQuery';

export type PageItem = {
	path: PagePath
	name?: string
	needLogin?: boolean
	meta?: UTSJSONObject
	query?: UTSJSONObject
	data?: UTSJSONObject
	style?: UTSJSONObject
};

/**
* 页面样式类型映射
*/
export type NavigationBarTextStyle = 'white' | 'black'
export type NavigationBarShadowColorType = 'grey' | 'blue' | 'green' | 'orange' | 'red' | 'yellow'
export type NavigationBarShadow = {
	colorType: NavigationBarShadowColorType
}
export type NavigationStyle = 'default' | 'custom'
export type PageOrientation = 'auto' | 'portrait' | 'landscape'
export type TransparentTitle = 'always' | 'auto' | 'none'
export type TitlePenetrate = 'YES' | 'NO'
export type CustomButtonType = 'forward' | 'back' | 'share' | 'favorite' | 'home' | 'menu' | 'close' | 'none'
export type CustomButton = {
	type?: CustomButtonType
	color?: string
	background?: string
	badgeText?: string
	colorPressed?: string
	float?: string
	fontWeight?: string
	fontSize?: string
	fontSrc?: string
	select?: string
	text?: string
	width?: string
}
export type SearchInput = {
	autoFocus?: boolean
	align?: string
	backgroundColor?: string
	borderRadius?: string
	placeholder?: string
	placeholderColor?: string
	disabled?: boolean
}
export type TitleNView = {
	backgroundColor?: string
	buttons?: CustomButton[]
	titleColor?: string
	titleText?: string
	titleSize?: string
	type?: string
	searchInput?: SearchInput
}
export type PullToRefreshType = 'defaultValue' | 'circle'
export type PullToRefreshContentcommon = {
	caption?: string
}
export type PullToRefresh = {
	support?: boolean
	color?: string
	type?: PullToRefreshType
	height?: string
	range?: string
	offset?: string
	contentdown?: PullToRefreshContentcommon
	contentover?: PullToRefreshContentcommon
	contentrefresh?: PullToRefreshContentcommon
}
export type H5 = {
	titleNView?: TitleNView
	pullToRefresh?: PullToRefresh
}
export type PageStyle = {
	navigationBarBackgroundColor?: string
	navigationBarTextStyle?: NavigationBarTextStyle
	navigationBarTitleText?: string
	navigationBarShadow?: NavigationBarShadow
	navigationStyle?: NavigationStyle
	backgroundColor?: string
	backgroundColorContent?: string
	enablePullDownRefresh?: boolean
	onReachBottomDistance?: number
	pageOrientation?: PageOrientation
	disableSwipeBack?: boolean
	titleImage?: string
	transparentTitle?: TransparentTitle
	titlePenetrate?: TitlePenetrate
	h5?: H5
	usingComponents?: UTSJSONObject
	leftWindow?: boolean
	topWindow?: boolean
	rightWindow?: boolean
	maxWidth?: number
}
export type AnimationType = 'slide-in-right' | 'slide-in-left' | 'slide-in-top' | 'slide-in-bottom' | 'fade-in' | 'zoom-out' | 'zoom-fade-out' | 'pop-in'
export type GlobalStyle = {
	navigationBarBackgroundColor?: string
	navigationBarTextStyle?: NavigationBarTextStyle
	navigationBarTitleText?: string
	navigationStyle?: NavigationStyle
	backgroundColor?: string
	backgroundColorContent?: string
	enablePullDownRefresh?: boolean
	onReachBottomDistance?: number
	titleImage?: string
	transparentTitle?: TransparentTitle
	titlePenetrate?: TitlePenetrate
	pageOrientation?: PageOrientation
	animationType?: AnimationType
	usingComponents?: UTSJSONObject
	leftWindow?: boolean
	topWindow?: boolean
	rightWindow?: boolean
	rpxCalcMaxDeviceWidth?: number
	rpxCalcBaseDeviceWidth?: number
	rpxCalcIncludeWidth?: number
	maxWidth?: number
	h5?: UTSJSONObject
}

export type IconFont = {
	text?: string
	selectedText?: string
	fontSize?: string
	color?: string
	selectedColor?: string
}
export type TabbarItem = {
	iconfont?: IconFont
	pagePath?: string
	text?: string
	iconPath?: string
	selectedIconPath?: string
	visible?: boolean
}
export type TabbarMidButton = {
	width?: string
	height?: string
	text?: string
	iconPath?: string
	iconWidth?: string
	backgroundImage?: string
}
export type Tabbar = {
	color?: string
	selectedColor?: string
	backgroundColor?: string
	borderStyle?: string
	blurEffect?: string
	list: TabbarItem[]
	fontSize?: string
	iconWith?: string
	spacing?: string
	height?: string
	midButton?: TabbarMidButton
	iconfontSrc?: string
	backgroundImage?: string
	backgroundRepeat?: string
	redDotColor?: string
}
export type MatchMedia = {
	minWidth?: number
}
export type CommonWindow = {
	path?: string
	style?: UTSJSONObject
	matchMedia?: MatchMedia
}
export type ConditionItem = {
	name: string
	path: string
	query?: string
}
export type Condition = {
	current: number
	list: ConditionItem[]
}
export type Easycom = {
	autoscan?: boolean
	custom?: UTSJSONObject
}
export type UniIdRouter = {
	loginPage?: string
	resToLogin?: boolean
	needLogin?: string[]
}
export const pages: PageItem[] = [
	{
		path: '/pages/index/index',
		name: 'PagesIndexIndex',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"uni-app x"}
	},
	{
		path: '/pages/p1/p1',
		name: 'PagesP1P1',
		needLogin: false,
		meta: {"a":2},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"","enablePullDownRefresh":false,"navigationStyle":"custom"}
	},
	{
		path: '/pages/p2/p2',
		name: 'p2',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"","enablePullDownRefresh":false}
	},
	{
		path: '/pages/demo/pageCompInterceptor/pageCompInterceptor',
		name: 'PagesDemoPageCompInterceptorPageCompInterceptor',
		needLogin: false,
		meta: {},
		query: {"a":1},
		data: {},
		style: {"navigationBarTitleText":"","navigationStyle":"custom"}
	},
	{
		path: '/pages/p1/login',
		name: 'PagesP1Login',
		needLogin: false,
		meta: {},
		query: {},
		data: {"b":2},
		style: {"navigationBarTitleText":""}
	},
	{
		path: '/pages/demo/tabbarInterceptor/tabbarInterceptor',
		name: 'PagesDemoTabbarInterceptorTabbarInterceptor',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"","enablePullDownRefresh":false,"navigationStyle":"custom"}
	},
	{
		path: '/pages/demo/tabbarInterceptor/login',
		name: 'PagesDemoTabbarInterceptorLogin',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":""}
	},
	{
		path: '/pages/demo/indexInterceptor/indexInterceptor',
		name: 'PagesDemoIndexInterceptorIndexInterceptor',
		needLogin: true,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"","enablePullDownRefresh":false,"navigationStyle":"custom"}
	},
	{
		path: '/pages/demo/indexInterceptor/login',
		name: 'PagesDemoIndexInterceptorLogin',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"","enablePullDownRefresh":false}
	},
	{
		path: '/pages/demo/routeInfo/routeInfo',
		name: 'PagesDemoRouteInfoRouteInfo',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"查看路由信息示例","enablePullDownRefresh":false,"navigationStyle":"custom"}
	},
	{
		path: '/pages/p2/login',
		name: 'PagesP2Login',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":""}
	},
	{
		path: '/pages/test1/test1',
		name: 'PagesTest1Test1',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":""}
	},
	{
		path: '/pages/test2/test2',
		name: 'PagesTest2Test2',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":""}
	},
	{
		path: '/pages/profile/profile',
		name: 'PagesProfileProfile',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"我的"}
	},
	{
		path: '/pages/demo/syncModalInterceptor/syncModalInterceptor',
		name: 'PagesDemoSyncModalInterceptorSyncModalInterceptor',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"异步弹窗拦截演示"}
	},
	{
		path: '/pages/demo/syncModalInterceptor/login',
		name: 'PagesDemoSyncModalInterceptorLogin',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"异步弹窗拦截登录页面"}
	},
	{
		path: '/pages/tabint/tabint',
		name: 'PagesTabintTabint',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":""}
	},
	{
		path: '/pages/demo/query/query',
		name: 'PagesDemoQueryQuery',
		needLogin: false,
		meta: {},
		query: {},
		data: {},
		style: {"navigationBarTitleText":"query传参测试"}
	},
];

export const globalStyle = {
	navigationBarBackgroundColor: '#F8F8F8',
	navigationBarTextStyle: 'black',
	backgroundColor: '#F8F8F8',
} as GlobalStyle

export const hasTabBar = true
export const tabBar = {
	list: [
		{
			pagePath: 'pages/index/index',
			text: '首页',
		},
		{
			pagePath: 'pages/tabint/tabint',
			text: '拦截p2',
		},
		{
			pagePath: 'pages/profile/profile',
			text: '我的',
		},
	] as TabbarItem[],
	fontSize: '20px',
	height: '100px',
} as Tabbar

export const hasTopWindow = false
export const hasLeftWindow = false
export const hasRightWindow = false
export const hasCondition = false
export const hasEasycom = true
export const easycom = {
	autoscan: true,
	custom: {"(.*)-box":"@/components/mybox/$1-box/$1-box.vue"}
} as Easycom

export const hasUniIdRouter = true
export const uniIdRouter = {
} as UniIdRouter
