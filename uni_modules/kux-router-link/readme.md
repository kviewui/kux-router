# kux-router-link
`kux-router-link` 是 [kux-router](https://ext.dcloud.net.cn/plugin?id=15998) 路由库配套使用的导航组件插件，方便通过组件方式导航守卫跳转。
> **注意**
> 
> + `kux-router-link` 插件同样支持 [kux-router](https://ext.dcloud.net.cn/plugin?id=15998) 插件的导航守卫跳转。
> + 需要 `kux-router` 版本 `v1.1.0` 及以上支持。
> + 该组件为 [easycom 规范](https://uniapp.dcloud.net.cn/component/#easycom) 组件，不需要手动导入可直接使用。

## 基础使用
### 示例
```
<template>
	<kux-router-link to="/pages/p1/p1">
		<button>打开页面p1</button>
	</kux-router-link>
</template>
```

### 属性
| 名称 | 类型 | 默认值 | 描述
| --- | --- | --- | ---
| to | string([string.PageURIString](https://doc.dcloud.net.cn/uni-app-x/uts/data-type.html#ide-string)) | - | 要前往的页面地址，支持带query参数，如：/pages/user/info?id=1
| openType | `'push' | 'replace' | 'switchTab' | 'reLaunch'` | 'push' | 导航方式，见下方说明
| options | `RouteRecordNormalized` | - | 编程式导航页面信息，在导航跳转时以对象方式设置更多参数信息，如 `data` 隐式传参，`meta` 元信息等等。见下发说明

### OpenType 说明
| 名称 | 说明
| --- | ---
| push | 对应 `router.push()`
| replace | 对应 `router.replace()`
| back |  对应 `router.back()`，`v1.0.1` 及以上版本该类型已废弃
| switchTab | `router.switchTab()`
| reLaunch | 对应 `router.reLaunch()`

### Options 类型说明
```
/**
 * 创建路由时路由表中的路由信息
 */
export type RouteRecordNormalized = {
	/**
	 * 被注册的 `beforeEnter` 前置守卫
	 */
	beforeEnter?: NavigationGuardWithThis
	// children: any[]
	/**
	 * 路由元信息
	 */
	meta?: UTSJSONObject
	/**
	 * 路由名称
	 */
	name?: RouteRecordName
	/**
	 * 当前路由页面地址，这个是去除参数后的地址，如：`/pages/user/info`
	 */
	path?: PagesJson.PagePath
	/**
	 * 当前路由的 `query` 传参
	 */
	query?: UTSJSONObject
	/**
	 * 路由隐式传参，替代旧版本的 `params`
	 */
	data?: UTSJSONObject
	/**
	 * 被注册的路由重定向守卫
	 */
	redirect?: RouteRecordRedirectOption
	/**
	 * 是否开启启动拦截，开启后 `beforeEnter` 和 `redirect` 会在实例加载成功后就立刻挂载全局，适用于应用启动拦截的场景，如首页未登录直接跳转登录页等。
	 */
	startupIntercept?: boolean
	/**
	 * 窗口显示的动画类型，`v1.1.0` 及以上版本支持
	 * + auto: 自动选择动画效果
	 * + none: 无动画效果
	 * + slide-in-right: 从右侧横向滑动效果
	 * + slide-in-left: 左侧横向滑动效果
	 * + slide-in-top: 从上侧竖向滑动效果
	 * + slide-in-bottom: 从下侧竖向滑动效果
	 * + fade-in: 从透明到不透明逐渐显示效果
	 * + zoom-out: 从小到大逐渐放大显示效果
	 * + zoom-fade-out: 从小到大逐渐放大并且从透明到不透明逐渐显示效果
	 * + pop-in: 从右侧平移入栈动画效果
	 */
	animationType?: string
	/**
	 * 窗口显示动画的持续时间，单位为 ms
	 * + `v1.1.0` 及以上版本支持
	 */
	animationDuration?: number
}
```

---
### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

___
### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手
+ [OneUI](https://ext.dcloud.net.cn/plugin?id=17104)：致力于提供简洁、现代的 UI 组件，帮助开发者快速构建美观、功能丰富的移动应用程序。
