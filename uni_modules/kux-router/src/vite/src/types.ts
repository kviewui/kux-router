/**
 * vite插件注册的配置项
 */
export interface VitePluginOption {
	/**
	 * pages.json映射文件路径
	 */
	pagesJsonMappingFile?: string
	/**
	 * 路由管理文件路径
	 */
	routerFile?: string
}