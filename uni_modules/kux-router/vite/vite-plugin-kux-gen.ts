import { genRouter, genInterface } from "./src/gen";
import { VitePluginOption } from './src/types'

export default function kuxGen(option?: VitePluginOption) {
	return {
		name: 'kux-gen',
		async config (config) {
			// genPages(option)
			genRouter(option)
			genInterface(option)
		}
	}
}