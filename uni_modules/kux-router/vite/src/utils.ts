import { VitePluginOption } from './types'
import { PAGES_JSON_MAPPING_FILE } from './constant'

const fs = require('fs');
const path = require('path');

export function removeLastPipe(input : string) : string {
	// 使用正则表达式找到最后一个 ' | ' 分隔符
	const lastPipeIndex = input.lastIndexOf(' | ');
	// 确保存在最后一个 ' | ' 分隔符
	if (lastPipeIndex !== -1) {
		// 删除最后一个 ' | ' 分隔符
		return input.slice(0, lastPipeIndex);
	} else {
		// 如果不存在最后一个 ' | ' 分隔符，返回原始字符串
		return input;
	}
}

export function toPascalCase(str : string) : string {
	// 分割字符串，根据'/'或者'-'或者'_'进行分割
	const parts = str.split(/[\s_/-]+/);
	// 将每个部分的首字母大写，然后拼接起来
	const pascalCased = parts.map(part => {
		return part.charAt(0).toUpperCase() + part.slice(1);
	}).join('');
	return pascalCased;
}

export const getNowDate = () => {
	const timestamp = Date.now();
	return new Date(timestamp);
}

export function formatTime(date : Date = getNowDate()) : string {
	const year : number = date.getFullYear();
	const month : number = date.getMonth() + 1; // 月份是从0开始的
	const day : number = date.getDate();
	const hours : number = date.getHours();
	const minutes : number = date.getMinutes();
	const seconds : number = date.getSeconds();

	// 格式化数字，确保它们前面有前导零
	const formattedMonth : string = month < 10 ? `0${month}` : month.toString();
	const formattedDay : string = day < 10 ? `0${day}` : day.toString();
	const formattedHours : string = hours < 10 ? `0${hours}` : hours.toString();
	const formattedMinutes : string = minutes < 10 ? `0${minutes}` : minutes.toString();
	const formattedSeconds : string = seconds < 10 ? `0${seconds}` : seconds.toString();

	// 组合成你想要的格式
	return `${year}-${formattedMonth}-${formattedDay} ${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
}

export function isDir(path : string) : boolean {
	try {
		return fs.statSync(path).isDirectory();
	} catch (e) {
		return false;
	}
}

export function resolvePath(fileName : string) : string {
	return path.resolve(process.env.UNI_INPUT_DIR, fileName);
}

export function autoMkdir(pathString : string) : void {
	if (isDir(path) && !fs.existsSync(dir)) {
		fs.mkdirSync(path, { recursive: true });
	} else {
		const dir = path.dirname(pathString);
		if (!fs.existsSync(dir)) {
			fs.mkdirSync(dir, { recursive: true });
		}
	}
}

export function resolvePagesJsonOutputPath(option : VitePluginOption, isModulePath = false) : string {
	let outputPath = PAGES_JSON_MAPPING_FILE;
	
	if (option?.pagesJsonMappingFile && option.pagesJsonMappingFile != PAGES_JSON_MAPPING_FILE) {
		if (isModulePath) {
			if (isDir(resolvePath(option.pagesJsonMappingFile))) {
				outputPath = `${option.pagesJsonMappingFile}/${PAGES_JSON_MAPPING_FILE}`
			} else {
				outputPath = option.pagesJsonMappingFile
			}
		} else {
			if (isDir(resolvePath(option.pagesJsonMappingFile))) {
				outputPath = resolvePath(`${option.pagesJsonMappingFile}/${PAGES_JSON_MAPPING_FILE}`)
			} else {
				outputPath = resolvePath(option.pagesJsonMappingFile)
			}
		}
	} else {
		if (!isModulePath) {
			outputPath = resolvePath(PAGES_JSON_MAPPING_FILE)
		}
	}
	
	return outputPath;
}

export async function copyFileWithOverwrite(src, dest) {
	try {
		// 检查目标文件是否存在
		await fs.access(dest);
		// 文件存在，执行删除
		await fs.unlink(dest);
	} catch (err) {
		if (err.code !== 'ENOENT') {
			throw err;
		}
	}
	
	// 执行复制操作
	await fs.copyFile(src, dest);
	console.log('File copied successfully to:', dest);
}

export function copyFile(src, dest, callback) {
	// 检查目标文件是否存在，如果存在则删除
	  fs.access(dest, (err) => {
	    if (!err) {
	      // 文件存在，执行删除操作
	      fs.unlink(dest, (unlinkErr) => {
	        if (unlinkErr) {
	          return callback(unlinkErr);
	        }
	        // 删除成功，执行复制操作
	        fs.copyFile(src, dest, callback);
	      });
	    } else if (err.code === 'ENOENT') {
	      // 文件不存在，直接执行复制操作
	      fs.copyFile(src, dest, callback);
	    } else {
	      // 访问文件时发生其他错误
	      callback(err);
	    }
	  });
}