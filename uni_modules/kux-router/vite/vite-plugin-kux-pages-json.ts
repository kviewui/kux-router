import { genPages, genRouter, genInterface } from "./src/gen";
import { VitePluginOption } from './src/types'

export default function pagesJsonPlugin(option?: VitePluginOption) {
	return {
		name: 'kux-pages-json',
		async config (config) {
			genPages(option)
			genRouter(option)
			genInterface(option)
		},
		transform (code, id) {
			if (id.endsWith('pages-json-uts') || id.endsWith('pages-json-js')) {
				genPages(option)
			}
		}
	}
}