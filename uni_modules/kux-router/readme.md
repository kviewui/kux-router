# kux-router
`kux-router` 是一个参考 `vue-router` 的api设计实现的 `uts` 路由库，支持 `vue-router` 的绝大数功能特色，如全局路由自定义 `routes` 配置，`beforeEach`、`afterEach` 等全局导航守卫，路由元信息等丰富的 api 供开发者灵活使用。
> ***注意：***<br/>
> 由于 `uts` 为强类型语言，所以涉及自定义传参的都要手动指定类型，所有参数类型都已经导出，开发者直接根据提示直接导入使用即可。
> 
> `v1.1.0`  为全新重构的版本，旧版本升级时请仔细查看版本更新日志，升级过程有什么问题可以进群交流：[870628986](https://qm.qq.com/q/lJOzzu6UEw)
> 
> `v1.1.5` 及以上版本开始支持 `uni` 全局挂载方式。[参考 `uni` 全局挂载](#advanced_uniext)

## 插件特色
+ 完全对齐 [vue-router](https://router.vuejs.org/zh/) 的API设计用法。
+ 支持多种导航守卫。
+ 支持应用启动拦截守卫。
+ 支持路由返回守卫。
+ 支持异步拦截守卫。
+ 支持组件式用法，需下载 [kux-router-link](https://ext.dcloud.net.cn/plugin?id=17593)。
+ 自动生成 `pages.json` 路由配置映射。
+ 支持 [uni ext api](https://uniapp.dcloud.net.cn/api/extapi.html)用法。【需升级插件到v1.1.5或以上版本】
+ ...其他更多优秀的工具方法。

## 官网全新上线：[https://router.uvuejs.cn](https://router.uvuejs.cn)

## 开源地址：[https://gitcode.com/kviewui/kux-router](https://gitcode.com/kviewui/kux-router)

## 目录结构
<div style="list-style-type: none;">
	<ol style="list-style-type: none;">
		<li><a href="#guide">基础</a>
			<ol>
				<li><a href="#guide_installation">安装配置</a></li>
				<li><a href="#guide_guide">入门使用</a></li>
				<li><a href="#guide_navigation">编程式导航</a></li>
				<li><a href="#guide_name-routes">命名路由</a></li>
				<li><a href="#guide_redirect">重定向</a></li>
			</ol>
		</li>
		<li><a href="#advanced">进阶</a>
			<ol>
				<li><a href="#advanced_navigation-guards">导航守卫</a></li>
				<li><a href="#advanced_meta">路由元信息</a></li>
				<li><a href="#advanced_composition-api">组合式API</a></li>
				<li><a href="#advanced_router_link">RouterLink</a></li>
				<li><a href="#advanced_uniext">uni全局挂载</a></li>
				<li><a href="#advanced_uni-router-map">uni路由映射</a></li>
			</ol>
		</li>
		<li><a href="#api">API参考</a>
			<ol>
				<li><a href="#api_functions">函数</a>
					<ol>
						<li><a href="#api_functions_createRouter">createRouter</a></li>
						<li><a href="#api_functions_useRouter">useRouter</a></li>
						<li><a href="#api_functions_useRoute">useRoute</a></li>
					</ol>
				</li>
				<li><a href="#api_types">自定义类型</a>
					<ol>
						<li><a href="#api_types_RouterOptions">RouterOptions</li>
						<li><a href="#api_types_RouteRecordRedirect">RouteRecordRedirect</li>
						<li><a href="#api_types_RouteRecordNormalized">RouteRecordNormalized</li>
						<li><a href="#api_types_RouteLocationRaw">RouteLocationRaw</li>
						<li><a href="#api_types_RouteLocationGuardReturnOptions">RouteLocationGuardReturnOptions</li>
						<li><a href="#api_types_NavigationGuardReturn">NavigationGuardReturn</li>
						<li><a href="#api_types_RouteLocationNormalizedLoaded">RouteLocationNormalizedLoaded</li>
						<li><a href="#api_types_NavigationGuardWithThis">NavigationGuardWithThis</li>
						<li><a href="#api_types_NavigationHookAfter">NavigationHookAfter</li>
						<li><a href="#api_types_RouteRecordName">RouteRecordName</a></li>
						<li><a href="#api_types_RouteRecordPath">RouteRecordPath</a></li>
					</ol>
				</li>
			</ol>
		</li>
	</ol>
</div>

<a id="guide"></a>
## 基础

<a id="guide_installation"></a>
### 安装配置
本插件为完全的 `uni_modules` 插件，所以直接在 [插件市场](https://ext.dcloud.net.cn) 搜索 `kux-router` 安装即可。

+ 项目根目录创建 `vite.config.ts`， 配置内容如下：

	```ts
	import { defineConfig } from 'vite';
	import uni from "@dcloudio/vite-plugin-uni";
	import autopages from './uni_modules/kux-autopages';
	import kuxgen from './uni_modules/kux-router/vite/vite-plugin-kux-gen';
	
	export default defineConfig({
		plugins: [
			uni(),
			autopages({
				pagesJsonMappingFile: 'router/pages.uts',
			}),
			kuxgen({
				pagesJsonMappingFile: 'router/pages.uts',
				routerFile: 'router/router.uts'
			}),
		],
	});
	```
	> **提示**
	>
	> + `v1.1.0` 及以下版本不需要配置这个。
	> + 项目根目录存在该文件时，不需要重新建创建该文件，直接按照上面示例配置 `pagesJsonPlugin ` 插件即可。
	> + `1.2.0` 版本开始需要绑定插件 [kux-autopages](https://ext.dcloud.net.cn/plugin?id=19799), 用来做 `pages.json` 类型映射使用, 如果下载插件时没有自动下载该绑定插件,需要去插件市场手动下载
	> + 按照以上配置编译后项目根目录会自动生成 `router` 目录, 其中目录下的 `pages.uts` 文件是根据 `pages.json` 自动生成的类型映射, `router.uts` 文件是自动生成的路由拦截管理文件

<a id="guide_guide"></a>
### 入门使用

#### `main.uts` 全局挂载
下载该 `uni_modules` 插件到自己的项目中后，由于新版的底层全面重构，所以在初始化的时候自动做了 [uni路由映射](https://router.uvuejs.cn/guide/advanced/uni-router-map.html)，可以直接全局挂载使用：
		
```
import App from './App.uvue'

import { createSSRApp } from 'vue'
import { createRouter, RouterOptions } from '@/uni_modules/kux-router';
import routes from './router/router';

export function createApp() {
	const app = createSSRApp(App)
	const router = createRouter({
		routes: routes
	} as RouterOptions);
	// 组合式API建议通过如下方式挂载
	app.provide('router', router);
	// 选项式API可以通过如下方式挂载
	// app.config.globalProperties.kuxRouter = router;
	return {
		app
	}
}
```

+ `v1.1.0` **以下版本用法**

	```uts
	import App from './App.uvue';
	
	import { createSSRApp } from 'vue';
	// 引入 kux-router 路由库
	import {createRouter} from '@/uni_modules/kux-router';
	
	export function createApp() {
		const app = createSSRApp(App);
		// 通过 app.use 全局挂载路由库，这里建议使用有意义的且不容易和全局内置变量冲突的名字，我测试中发现是不能使用 `router` `route` `$route` 这些名字
		app.config.globalProperties.kuxRouter = createRouter();
		return {
			app
		}
	}
	```
	
> **提示**	
>为了方便统一话术，上面的 `kuxRouter` 在下文中统称为 `router`，下文所有实例名称为 `router`，实际使用过程请参考自己全局挂载的方式和名称。

#### `uni` 全局挂载
请参考 [`uni` 全局挂载](#advanced_uniext)。

#### `uni` 路由映射
请参考 [uni路由映射](https://router.uvuejs.cn/guide/advanced/uni-router-map.html)

#### 页面调用
```uts
router.push('/pages/p1/p1');
```

<a id="guide_navigation"></a>
### 编程式导航
我们可以通过 `router` 的实例方法来自定义跳转到导航的页面。

#### 导航到不同的位置
想要导航到不同的 URL，可以使用 `router.push` 方法。这个方法会向路由栈添加一个新的记录。<br/>

+ `v1.1.0` **及以上版本用法**
	
	该方法有一个参数，参数类型支持多种，具体类型说明如下：
	
	| 类型名 | 默认值 | 说明
	| --- | --- | ---
	| [RouteRecordPath](#api_types_RouteRecordName) | - | 路由地址，该类型会在重新编译的时候根据pages.json自动更新
	| [RouteRecordNormalized](#api_types_RouteRecordNormalized) | - | 创建路由表时的路由配置信息
+ `v1.1.0` **以下版本用法**

	该方法的参数有两个，第一个参数为 `必填` 的页面路径，第二个参数为 `可选` 的路由配置项，结构如下：<br/>
	
	| 参数名 | 是否必填 | 类型 | 默认值 | 说明
	| --- | --- | --- | --- | ---
	| to | 是 | `string` | - | 即将访问的页面路径
	| options | 否 | `RouteLocationRaw` 见下方 | {} | 跳转页面时的配置项，如页面传参信息

> ***注意***<br/>
> 由于 `uts` 支持的联合类型有严格的限制，所以无法实现 `vue-router` 的那种可变类型传参，所以第一个参数必须填写。
> 
> `v1.1.0` 及以上版本由于底层进行了重构，所以参数支持多种类型。

#### RouteLocationRaw 说明
| 参数名 | 是否必填 | 类型 | 默认值 | 说明
| --- | --- | --- | --- | ---
| query | 否 | `UTSJSONObject` | {} | 页面 query 传参，会被路由库自动转为 `query string` 字符串参数拼接到页面地址后面

> ***注意***<br/>
> 由于 `uniapp x` 目前不支持自定义路由，也不支持动态路由，所以暂时不支持 `vue-router` 的 `params` 传参。

+ `v1.1.0` 及以上版本

	```
	// 路径直传
	router.push('/pages/user/list');
	
	// 对象方式跳转，可以指定 `query` 参数，`data` 参数等其他内容
	router.push({
		path: '/pages/user/info',
		query: {
			id: 1
		},
		data: {
			member_id: 30
		}
	} as RouteRecordNormalized);
	```
+  `v1.1.0` 以下版本

	```uts
	// 没有参数跳转
	router.push('/pages/user/list');
	
	// 带参数跳转，由于 uts 为强类型语言，`{}` 对象参数默认是 `UTSJSONObject` 类型，所以带参数时需要手动指定 `RouteLocationRaw ` 类型
	import {RouteLocationRaw} from '@/uni_modules/kux-router';
	 router.push('/pages/user/info', {
		query: {
			id: 1
		}
	} as RouteLocationRaw);
	```

#### 横跨历史
该方法采用一个整数作为参数，表示在历史堆栈中后退多少步，其实就是 [uni.navigateBack](https://doc.dcloud.net.cn/uni-app-x/api/navigator.html#navigateback) 。<br/>
例子

```uts
// 返回1条记录，与 uni.navigateBack() 相同
router.back();

// 返回 3 条记录，与 uni.navigateBack({delta: 3}) 相同
router.back(3);
```

<a id="guide_name-routes"></a>
### 命名路由
除了 `to` 之外，还可以通过路由的 `name` 进行跳转。主要应用于 `routes` 中的路由重定向。

```uts
import {RouteRecordNormalized} from '@/uni_modules/kux-router';
const routes = [
	{
		path: '/pages/user/info',
		name: 'UserInfo'
	}
] as RouteRecordNormalized[];
```

要跳转到一个命名的路由，可以参考 [重定向](#guide_redirect) 介绍。

<a id="guide_redirect"></a>
### 重定向
重定向也是通过 `routes` 配置来完成，下面例子是从 `/home` 重定向到 `/`：

+ `v1.1.0` 及以上版本

	```
	import {RouteRecordNormalized} from '@/uni_modules/kux-router';
	const routes = [
		{
			path: '/home',
			redirect: (_): RouteRecordNormalized | null => {
				return {
					path: '/'
				} as RouteRecordNormalized;
			}
		},
		{
			path: '/'
		}
	] as RouteRecordNormalized[];
	```

+ `v1.1.0` 以下版本

	```uts
	import {RouteRecordNormalized, RouteRecordRedirect} from '@/uni_modules/kux-router';
	const routes = [
		{
			path: '/home',
			redirect: (_): RouteRecordRedirect => {
				return {
					path: '/'
				} as RouteRecordRedirect;
			}
		},
		{
			path: '/'
		}
	] as RouteRecordNormalized;
	```

重定向的目标也可以是一个命名的路由：

+ `v1.1.0` 及以上版本

	```
	import {RouteRecordNormalized} from '@/uni_modules/kux-router';
	import { state } from '@/store/index';
	
	routes.push({
		path: '/pages/p1/p1',
		name: item.name,
		query: {
			e: 1,
			f: 2
		} as UTSJSONObject,
		redirect: (to): RouteRecordNormalized | null => {
			// 测试判断是否登录
			if (state.token1.length == 0) {
				return {
					name: 'PagesP1Login'
				} as RouteRecordNormalized;
			}
			
			return null;
		}
	} as RouteRecordNormalized);
	```
+ `v1.1.0` 以下版本

	```uts
	import {RouteRecordNormalized, RouteRecordRedirect} from '@/uni_modules/kux-router';
	const routes = [
		{
			path: '/home',
			redirect: (_): RouteRecordRedirect => {
				return {
					name: 'homepage'
				} as RouteRecordRedirect;
			}
		},
		{
			path: '/homepage',
			name: 'homepage'
		}
	] as RouteRecordNormalized;
	
	```

> ***注意***<br/>
> 1.重定向的路由必须要在 `routes` 中配置过才可以，如示例上面的配置即可。<br/>
> 2.目前重定向是通过 `redirect` 动态方法实现，因为 `uts` 不支持自定义的联合类型，所以无法实现 `vue-router` 中的字符串路径参数进行重定向。

<a id="advanced"></a>
## 进阶

<a id="advanced_navigation-guards"></a>
### 导航守卫
`kux-router` 提供的导航守卫主要用来通过跳转或取消的方法守卫导航。目前支持全局导航守卫和路由配置全局守卫两种方式。

#### 全局前置守卫
你可以使用 `router.beforeEach` 注册一个全局前置守卫：

+  `v1.1.0` 及以上版本

	```
	import { RouteRecordNormalized } from '@/uni_modules/kux-router';
	
	router.beforeEach(async (to, from): Promise<any | null> => {
		if (from?.path == '/home') {
			return {
				path: '/homepage'
			} as RouteRecordNormalized;
		}
		
		return null;
	})
	```
	> **提示**
	>
	> `beforeEach` 返回一个可以用来移除自身的函数，如果需要移除该守卫，可以在使用完守卫后直接调用自身函数，示例代码如下：
	>
	> ```
	>  router.beforeEach(async (to, from): Promise<any | null> => {
	>	if (from?.path == '/home') {
	>		return {
	>			path: '/homepage'
	>		} as RouteRecordNormalized;
	>	}
	>	
	>	return null;
	> })();  // 这里最后调用自身即可移除该守卫
	> ```

+ `v1.1.0` 以下版本

	```uts
	import {NavigationGuardReturn, RouteLocationGuardReturnOptions} from '@/uni_modules/kux-router';
	router.beforeEach(async (to, from) => {
		// 这里可以根据 to 和 from 信息来灵活跳转页面
		if (from.path === '/home') {
			return {
				to: {
					path: '/homepage'
				} as RouteLocationGuardReturnOptions
			} as NavigationGuardReturn
		}
	});
	```
	> ***注意***<br/>
	> 因为 `uts` 强类型语言，不支持自定义联合类型，所以无法实现 `vue-router` 那种异步和同步同时共用一个参数位置的形式，目前只能统一使用 `async` 同步拦截方式，后面会提供异步拦截的 API

#### 全局后置钩子
你也可以注册全局后置钩子，和前置守卫不同的是后置钩子不需要返回类型，一般用来做路由跳转后的日志统计信息。

```uts
router.push('/pages/p1/p1');
// 模拟路由后置钩子记录日志
router.afterEach((to, from) => {
	console.log(`页面 ${from.path} 跳转到了页面 ${to.path}`);
});
```
> **提示**
> 
> `v1.1.0` 及以上版本该守卫返回一个用来移除自身的函数，可以直接调用自身来移除该守卫。

#### 路由独享的守卫
你可以直接在路由配置上定义 `beforeEnter` 守卫：

+ `v1.1.0` 及以上版本

	```
	import { RouteRecordNormalized } from '@/uni_modules/kux-router';
	const routes = [
		{
			path: '/pages/p5/p5',
			beforeEnter: (async (_, _): Promise<any | null> => {
				return {
					path: '/pages/p6/p6'	
				} as RouteRecordNormalized;
			})
		}
	] as RouteRecordNormalized[];
	```
+ `v1.1.0` 以下版本

	```uts
	import {RouteRecordNormalized, RouteLocationGuardReturnOptions, NavigationGuardReturn} from '@/uni_modules/kux-router';
	const routes = [
		{
			path: '/pages/p5/p5',
			beforeEnter: (async (_, _): Promise<NavigationGuardReturn> => {
				return {
					to: {
						path: '/pages/p6/p6'
					} as RouteLocationGuardReturnOptions
				} as NavigationGuardReturn;
			})
		}
	] as RouteRecordNormalized[];
	```

#### 完整的守卫解析流程
+ 检测 `routes` 配置中是否有 `redirect` 重定向，有的话执行重定向。
+ 检测 `routes` 配置中是否有 `beforeEnter` 守卫，有的话执行守卫。
+ 检测全局的 `beforeEach` 守卫，如果定义了就执行守卫。
+ 检测全局的 `afterEach` 守卫，如果定义了就执行守卫。

<a id="advanced_meta"></a>
### 路由元信息
有时你希望将自定义的任意信息添加到路由上，如是否需要授权的场景，通过路由配置对象的 `meta` 属性可以实现前面的需求，并且它会被记录在全局路由配置状态中，路由信息和导航守卫都可以访问到。定义路由的时候你可以这样配置 `meta` 字段：

```uts
import {RouteRecordNormalized} from '@/uni_modules/kux-router';
const routes = [
	{
		path: '/pages/posts/new',
		meta: {
			// 只有经过身份认证的用户才能创建帖子
			requiresAuth: true
		}
	},
	{
		path: '/pages/posts/info',
		meta: {
			// 任何人都可以阅读
			requiresAuth: false
		}
	}
] as RouteRecordNormalized[];
```

这时候你可以通过 `beforeEach` 全局前置守卫来检查是否授权

+ `v1.1.0` 及以上版本
	
	```
	import { RouteRecordNormalized } from '@/uni_modules/kux-router';
	
	router.beforeEach(async (to, from): Promise<any | null> => {
		if (to?.meta?.get('requiresAuth') == true && !auth.isLoggedIn()) {
			// 此路由需要授权，请检查是否已登录
			// 如果没有，则重定向到登录页面
			return {
				path: '/pages/login/index',
				// 保存现在的位置，方便后面跳回来
				query: {
					redirect: to.fullPath
				}
			} as RouteRecordNormalized;
		}
		
		return null;
	});
	```
+ `v1.1.0` 以下版本

	```uts
	import {NavigationGuardReturn, RouteLocationGuardReturnOptions} from '@/uni_modules/kux-router';
	
	router.beforeEach(async (to, from): Promise<NavigationGuardReturn> => {
		if (to.meta.requiresAuth && !auth.isLoggedIn()) {
			// 此路由需要授权，请检查是否已登录
			// 如果没有，则重定向到登录页面
			return {
				to: {
					path: '/pages/login/index',
					// 保存现在的位置，方便后面跳回来
					query: {
						redirect: to.fullUrl
					}
				} as RouteLocationGuardReturnOptions
			} as NavigationGuardReturn;
		}
	});
	```

<a id="advanced_composition-api"></a>
### 组合式API

#### 访问路由和当前路由
虽然推荐使用方式是 `main.uts` 全局挂载，不过路由库还是像 `vue-router` 一样提供了 `useRouter` 和 `useRoute` 函数：

```uts
import {useRouter, useRoute} from '@/uni_modules/kux-router';

const router = useRouter();
const route = useRoute();

router.push('/pages/user/info');

// 获取当前路由信息
console.log(route);
```

> ***注意***<br/>
> 通过 `router.currentRoute()` 也能获取到当前路由信息，和 `useRoute` 是完全一样的。

#### `useRoute` 路由信息参数说明
| 参数名 | 参数类型 | 说明
| --- | --- | --- | ---
| query | `UTSJSONObject` | 当前路由 `query` 参数
| params | `UTSJSONObject` | 当前路由 `params` 参数，该参数在 `v1.1.0` 及以上版本被废弃，替换为 `data` 属性
| path | `string` | 当前路由页面地址，这个是去除参数后的地址，如：`/pages/user/info`
| fullUrl | `string` | 当前路由页面带参数的完整地址，如：`/pages/user/info?id=1`，该参数在 `v1.1.0` 及以上版本被废弃，请替换为 `fullPath` 属性
| meta | `UTSJSONObject` | `routes` 配置中的路由元信息，如果没有自己配置，该参数为 `null`
| data`v1.1.0` | `UTSJSONObject` | 路由隐式传参，即路由参数不在地址栏中展现的参数，用来替代旧版本的 `params`
| fullPath`v1.1.0` | `string` | 该属性替代旧版本的 `fullUrl`
| name`v1.1.0` | [RouteRecordName](#api_types_RouteRecordName) | 路由名称
|  from`v1.1.0` | [RouteLocationNormalizedLoaded](#api_types_RouteLocationNormalizedLoaded) | 从哪个页面跳转过来的
| to`v1.1.0` | [RouteLocationNormalizedLoaded](#api_types_RouteLocationNormalizedLoaded) | 要到哪个页面去，一般用于路由拦截后完成某些操作再重定向到目标页面的场景

<a id="advanced_router_link"></a>
### RouterLink
`v1.1.0` 及以上版本提供了组件标签式的路由跳转导航守卫使用方式，需要单独下载 [kux-router-link](https://ext.dcloud.net.cn/plugin?id=17593) 插件。路由实例的所有导航守卫对该组件都生效。

#### 属性
| 名称 | 类型 | 默认值 | 描述
| --- | --- | --- | ---
| to | string([string.PageURIString](https://doc.dcloud.net.cn/uni-app-x/uts/data-type.html#ide-string)) | - | 要前往的页面地址，支持带query参数，如：/pages/user/info?id=1
| openType | `'push' | 'replace' | 'switchTab' | 'reLaunch'` | 'push' | 导航方式，见下方说明
| options | [RouteRecordNormalized](#api_types_RouteRecordNormalized) | - | 编程式导航页面信息，在导航跳转时以对象方式设置更多参数信息，如 `data` 隐式传参，`meta` 元信息等等。见下发说明

#### OpenType 说明
| 名称 | 说明
| --- | ---
| push | 对应 `router.push()`
| replace | 对应 `router.replace()`
| switchTab | `router.switchTab()`
| reLaunch | 对应 `router.reLaunch()`

#### 示例代码
```
<kux-router-link to="/pages/demo/pageCompInterceptor/pageCompInterceptor">
	<button type="primary">页面组件拦截示例</button>
</kux-router-link>
```

<a id="advanced_uniext"></a>
### `uni` 全局挂载
插件通过vite自动生成了 [uni ext api](https://uniapp.dcloud.net.cn/api/extapi.html) 的封装插件来实现了直接挂载 `uni` 对象。具体操作如下：

+ `vite.config.ts` 修改如下：

```typescript
import { defineConfig } from 'vite';
import uni from "@dcloudio/vite-plugin-uni";
import generateUniext from './uni_modules/kux-router/vite/vite-plugin-generate-uniext'
import pagesJsonPlugin from './uni_modules/kux-router/vite/vite-plugin-kux-pages-json.ts';

export default defineConfig({
	plugins: [
		generateUniext,
		uni(),
		pagesJsonPlugin
	],
})
```

+ 然后项目中即可直接使用： 
	+ `uni.useKuxRouter` 来替代原来的手动导入 `useRouter`。
	+ `uni.useKuxRoute` 来替代原来的手动导入 `useRoute`。
	+ `uni.createKuxRouter` 来替代原来的手动导入 `createRouter`。

> **提示**
>
> `v1.1.5` 及以上版本支持。
> 
> 上面的 `generateUniext` 为新增的导入模块。模块初始化时会在 `uni_modules` 目录下面生成 `uni-kuxrouter` 插件，该插件第一次生成时需要编译两次才能正常使用。该插件存在时按照正常的编译一次即可。
> 
> 介意上面第一次需要编译两次的谨慎使用 `uni` 全局挂载方式。

### `uni` 路由映射
<a id="advanced_uni-router-map"></a>
#### 文档: [uni路由映射](https://router.uvuejs.cn/guide/advanced/uni-router-map.html)


<a id="api"></a>
## API参考

<a id="api_functions"></a>
### 函数

<a id="api_functions_createRouter"></a>
#### createRouter
+ createRoute(`options`)：[KuxRouter](#api_interfaces_KuxRouter)


创建一个 [KuxRouter](#api_interfaces_KuxRouter) 实例。<br/>
> **提示**
> 
> `v1.1.0` 及以上版本实例名称为 `IRouter`

**参数**

| 名称 | 类型 | 描述
| --- | --- | ---
| `options` | [RouterOptions](#api_interfaces_RouterOptions) | [RouterOptions](#api_interfaces_RouterOptions) 

**返回值**

[KuxRouter](#api_interfaces_KuxRouter)

<a id="api_interfaces_RouterOptions"></a>
#### RouterOptions 说明
用来初始化一个 [KuxRouter](#api_interfaces_KuxRouter) 实例的选项。

**属性**

#### routes
+ routes：[RouteRecordNormalized](#api_interfaces_RouteRecordNormalized)[]

添加到路由器的初始路由列表。一般用来做 [路由元信息](#advanced_meta) 配置时才需要手动设置它。

+ debug: `boolean`

用来开启关闭路由库调试。

<a id="api_interfaces_ RouteRecordNormalized"></a>
#### RouteRecordNormalized 说明
`routes` 的路由配置信息。

**属性**

#### beforeEnter
+ beforeEnter?: [NavigationGuardWithThis](#api_interfaces_NavigationGuardWithThis)

被注册的 `beforeEnter` 守卫。

<a id="api_interfaces_NavigationGuardWithThis"></a>
#### NavigationGuardWithThis 说明
导航守卫。详情可看[导航守卫](#advanced_navigation-guards)。

**参数**

| 名称 | 类型
| --- | ---
| `to` | [RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)
| `from` | [RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)

**返回值**

`Promise<NavigationGuardReturn>`

<a id="api_interfaces_RouteLocationNormalizedLoaded"></a>
#### RouteLocationNormalizedLoaded 说明

**属性**

#### query
+ query：`UTSJSONObject`

当前路由的 `query` 传参。

#### params
+ params：`UTSJSONObject`

当前路由的 `params` 传参。目前没用到该属性。

#### path
+ path：`string`

当前路由去除 `query` 参数后的页面地址。

#### fullUrl
+ fullUrl：`string`

当前路由带 `query` 参数的完整页面地址。

#### meta
+ meta：`UTSJSONObject`

`routes` 配置中当前路由匹配的 [路由元信息](#advanced_meta)。

<a id="api_interfaces_KuxRouter"></a>
#### KuxRouter/IRouter 说明
路由器实例。

**属性**

#### options
+ options: `RouterOptions | null`

创建路由器时的原始选项对象。
> **提示**
> 
> `v1.1.0` 及以上版本支持。

#### from
+ from: `RouteLocationNormalizedLoaded | null`

获取即将离开的路由信息。
> **提示**
> 
> `v1.1.0` 及以上版本支持。

**方法**

#### addRoute
+ addRoute(`routeRecord`: [RouteRecordNormalized](#api_interfaces_RouteRecordNormalized))：() => `void`

动态增加routes配置中的路由信息。

***参数***

| 名称 | 类型 | 描述
| --- | --- | ---
| `routeRecord` | [RouteRecordNormalized](#api_interfaces_RouteRecordNormalized) | `routes` 的路由配置信息。

***返回值***

`void`

***说明***

`v1.1.0` 及以上版本该方法将被废弃，由路由库提供新的自动化解决方案替代。

#### currentRoute
+ currentRoute()：[RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)：() => [`RouteLocationNormalizedLoaded`](#api_interfaces_RouteLocationNormalizedLoaded)

***参数***

`void`

***返回值***

[RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)

#### afterEach
+ afterEach(`guard`): () => `void`

路由跳转后的守卫。

***参数***

| 名称 | 类型 | 描述
| --- | --- | ---
| `guard` | [NavigationHookAfter](#api_interfaces_NavigationHookAfter) | 要加入的导航钩子

<a id="api_interfaces_NavigationHookAfter"></a>
#### NavigationHookAfter
+ NavigationHookAfter(`to`, `from`): `void`

***参数***

| 名称 | 类型
| --- | ---
| `to` | [RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)
| `from` | [RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)

***返回值***

+ `v1.1.0` 及以上版本

	`() => void`
+ `v1.1.0` 以下版本

	`void`

***说明***

`v1.1.0` 及以上版本返回用来移除自身的函数。

#### beforeEach
+ beforeEach(`guard`): () => `void`

路由跳转前的守卫。

***参数***

| 名称 | 类型 | 描述
| --- | --- | ---
| `guard` | [NavigationGuardWithThis](#api_interfaces_NavigationGuardWithThis) | 要加入的导航钩子

***返回值***

+ `v1.1.0` 及以上版本

	`() => void`
+ `v1.1.0` 以下版本

	`void`

***说明***

`v1.1.0` 及以上版本返回用来移除自身的函数。


#### removeBeforeEach
+ removeBeforeEach(): () => `void`

移除全局的 `beforeEach` 路由跳转前的守卫。

***返回值***

`void`

#### updateRoute
+ updateRoute(`routeRecord`): () => `void`

动态更新routes配置中的路由信息。

***参数***

| 名称 | 类型 | 描述
| --- | --- | ---
| `routeRecord` | [RouteRecordNormalized](#api_interfaces_RouteRecordNormalized) | 增加的路由信息

***返回值***

`void`

#### hasRoute
+ hasRoute(`path`): () => `boolean`

判断路由是否存在。根据 `getCurrentPages` 返回的当前路由栈信息判断，和routes中的无关。

***参数***

| 名称 | 类型 | 描述
| --- | --- | ---
| `path` | `string` | 页面地址

***返回值***

`boolean`

***说明***

`v1.1.0` 及以上版本支持通过路由名称 `name` 判断<br/>
`v1.1.0` 及以上版本传页面地址时请去掉前面的 `/`，示例：`pages/index/index`

#### getRoutes
+ getRoutes(): () => [RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)[]

获取所有的路由信息。根据 `getCurrentPages` 返回的当前路由栈信息判断，和routes的信息无关。

***说明***

`v1.1.0` 及以上版本可以获取 routes 中配置的内容，以符合规范化路由信息标准

#### back
+ back(`delta`): () => `void`

关闭当前页面，返回上一页面或多级页面。同 `uni.navigateBack`

***参数***

| 名称 | 类型 | 描述
| --- | --- | ---
| `delta` | `number` | 返回的页面数，如果 `delta` 大于现有页面数，则返回到首页 

#### from
+ from(): () => [RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)

获取即将离开的路由信息。

***说明***

`v1.1.0` 及以上版本该方法被废弃，由 `from` 属性替代。

#### push
+ `v1.1.0` 及以上版本
	+ push(`to`): `Promise<NavigationFailure | null>`
	
		程序式地通过将一条记录加入到历史栈中来导航到一个新的 URL。
		
		***参数***
		
		| 名称 | 类型 | 描述
		| --- | --- | ---
		| `to` | [RouteRecordPath](#api_types_RouteRecordPath) | 要导航到的路由地址
		| `to` | [RouteRecordNormalized](#api_types_RouteRecordNormalized) | 要导航到的路由信息
		
		***返回值***
		
		`Promise<NavigationFailure | null>`
+ `v1.1.0` 以下版本
	+ push(`to`, `options`): () => `void`
	
		跳转到新的路由页面。
	
		***参数***
		
		| 名称 | 类型 | 描述
		| --- | --- | ---
		| `to` | `string` | 页面地址
		| `options` | `RouteLocationRaw` | 可选配置项
		
		***返回值***
		
		`void`

#### replace

+ replace(`to`): `Promise<NavigationFailure | null>`

	程序式地通过替换历史栈中的当前记录来导航到一个新的 URL。
	
	***参数***
	
	| 名称 | 类型 | 描述
	| --- | --- | ---
	| `to` | [RouteRecordPath](#api_types_RouteRecordPath) | 要导航到的路由地址
	| `to` | [RouteRecordNormalized](#api_types_RouteRecordNormalized) | 要导航到的路由信息
	
	***返回值***
	
	`Promise<NavigationFailure | null>`
	
	***说明***
	
	`v1.1.0` 及以上版本支持
	
#### reLaunch

+ reLaunch(`to`): `Promise<NavigationFailure | null>`

	关闭所有页面，打开到应用内的某个页面。
	
	***参数***
	
	| 名称 | 类型 | 描述
	| --- | --- | ---
	| `to` | [RouteRecordPath](#api_types_RouteRecordPath) | 要导航到的路由地址
	| `to` | [RouteRecordNormalized](#api_types_RouteRecordNormalized) | 要导航到的路由信息
	
	***返回值***
	
	`Promise<NavigationFailure | null>`
	
	***说明***
	
	`v1.1.0` 及以上版本支持
	
#### switchTab

+ switchTab(`to`): `Promise<NavigationFailure | null>`

	跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面。
	
	***参数***
	
	| 名称 | 类型 | 描述
	| --- | --- | ---
	| `to` | [RouteRecordPath](#api_types_RouteRecordPath) | 要导航到的路由地址
	| `to` | [RouteRecordNormalized](#api_types_RouteRecordNormalized) | 要导航到的路由信息
	
	***返回值***
	
	`Promise<NavigationFailure | null>`
	
	***说明***
	
	`v1.1.0` 及以上版本支持
	
#### onError

+ onError(handler: [ErrorListener](#api_types_ErrorListener)): void

	添加一个错误处理器，它会在每次导航遇到未被捕获的错误出现时被调用。其中包括同步和异步被抛出的错误、在任何导航守卫中返回或传入 next 的错误、尝试解析一个需要渲染路由的异步组件时发生的错误。
	
	***参数***
	
	| 名称 | 类型 | 描述
	| --- | --- | ---
	| handler | [ErrorListener](#api_types_ErrorListener) | 要注册的错误处理器
	
	***返回值***
	
	`void`
	
	***说明***
	
	`v1.1.0` 及以上版本支持
	
#### resolve

+ resolve(to: [RouteRecordName](#api_types_RouteRecordName)): `RouteLocationNormalizedLoaded | null`

	返回一个路由的规范化路由信息。
	
	***参数***
	
	| 名称 | 类型 | 描述
	| --- | --- | ---
	| to | [RouteRecordName](#api_types_RouteRecordName) | 路由名称
	
	***返回值***
	
	`RouteLocationNormalizedLoaded | null`
	
	***说明***
	
	`v1.1.0` 及以上版本支持

			
<a id="api_functions_useRouter"></a>
#### useRouter
+ useRouter()：[KuxRouter](#api_interfaces_KuxRouter)

返回当前路由器实例。

<a id="api_functions_useRoute"></a>
#### useRoute
+ useRoute (): [RouteLocationNormalizedLoaded](#api_interfaces_RouteLocationNormalizedLoaded)

返回当前路由信息。

### 自定义类型

<a id="api_types_RouteRecordRedirect"></a>
#### RouteRecordRedirect
```uts
export type RouteRecordRedirect = {
	path?: string;
	name?: string;
};
```

<a id="api_types_RouteRecordName"></a>
#### RouteRecordName
```
import * as PagesJson from '@/pagesJsonRouter.uts';

/**
 * 路由名称
 */
export type RouteRecordName = PagesJson.PageName
```
> **提示**
> 
> `v1.1.0` 及以上版本支持

<a id="api_types_RouteRecordPath"></a>
#### RouteRecordPath
```
import * as PagesJson from '@/pagesJsonRouter.uts';

/**
 * 路由地址
 */
export type RouteRecordPath = PagesJson.PagePath
```
> **提示**
> 
> `v1.1.0` 及以上版本支持

<a id="api_types_RouteLocationGuardReturnOptions"></a>
#### RouteLocationGuardReturnOptions
```
export type RouteLocationGuardReturnOptions = {
	/**
	 * 当前路由的 `query` 传参
	 */
	query?: UTSJSONObject
	// params?: Map<string, any>
	/**
	 * 当前路由去除 `query` 参数后的页面地址
	 */
	path?: string
	/**
	 * 当前路由隐式传参，即参数不在地址栏中展现
	 * + `v1.1.0` 及以上版本支持。
	 */
	data?: UTSJSONObject
	/**
	 * 路由名称，即 `routes` 中配置的 `name` 参数
	 */
	name?: RouteRecordName
	/**
	 * 路由元信息，即 `routes` 中配置的 `meta` 参数
	 */
	meta?: UTSJSONObject
	/**
	 * 当前路由页面带参数的完整地址，如：`/pages/user/info?id=1`
	 * + `v1.1.0` 及以上版本支持。
	 */
	fullPath?: string
};
```

<a id="api_types_NavigationGuardReturn"></a>
#### NavigationGuardReturn
```
export type NavigationGuardReturn = {
	/**
	 * 即将前往的路由信息
	 */
	to?: RouteLocationGuardReturnOptions
	/**
	 * 当前路由信息
	 */
	from?: RouteLocationGuardReturnOptions
};
```

<a id="api_types_RouteLocationNormalizedLoaded"></a>
#### RouteLocationNormalizedLoaded
```
/**
 * 导航完成后的规范化路由信息
 */
export type RouteLocationNormalizedLoaded = {
	/**
	 * 当前路由的 `query` 传参
	 */
	query?: UTSJSONObject
	/**
	 * @deprecated 该属性即将被废弃，请用 `data` 替代
	 */
	params?: UTSJSONObject
	/**
	 * 路由隐式传参，即路由参数不在地址栏中展现的参数，用来替代旧版本的 `params`
	 * + `v1.1.0` 及以上版本支持
	 */
	data?: UTSJSONObject
	/**
	 * 路由名称
	 * + `v1.1.0` 及以上版本支持，即 `routes` 中的 `name` 参数
	 */
	name?: RouteRecordName
	/**
	 * 当前路由页面地址，这个是去除参数后的地址，如：`/pages/user/info`
	 */
	path: string
	/**
	 * @deprecated 该属性即将被废弃，请用 `fullPath` 替代
	 */
	fullUrl?: string
	/**
	 * 该属性替代旧版本的 `fullUrl`
	 */
	fullPath?: string
	/**
	 * 路由元信息，即 `routes` 中的 `meta` 参数
	 */
	meta?: UTSJSONObject
	/**
	 * 从哪个页面跳转过来的
	 */
	from?: RouteLocationNormalizedLoaded
	/**
	 * 要到哪个页面去，一般用于路由拦截后完成某些操作再重定向到目标页面的场景
	 */
	to?: RouteLocationNormalizedLoaded
};
```

<a id="api_types_NavigationGuardWithThis"></a>
#### NavigationGuardWithThis
```
export type NavigationGuardWithThis = (to: RouteLocationNormalizedLoaded | null, from: RouteLocationNormalizedLoaded | null) => Promise<any | null>
```

<a id="api_types_RouteRecordRedirectOption"></a>
#### RouteRecordRedirectOption
```
export type RouteRecordRedirectOption = (to: RouteLocationNormalizedLoaded) => RouteRecordNormalized | null
```
> **提示**
> 
> `v1.1.0` 及以上版本支持。

<a id="api_types_RouteRecordNormalized"></a>
#### RouteRecordNormalized
```uts
/**
 * 创建路由时路由表中的路由信息
 */
export type RouteRecordNormalized = {
	/**
	 * 被注册的 `beforeEnter` 前置守卫
	 */
	beforeEnter?: NavigationGuardWithThis
	// children: any[]
	/**
	 * 路由元信息
	 */
	meta?: UTSJSONObject
	/**
	 * 路由名称
	 */
	name?: RouteRecordName
	/**
	 * 当前路由页面地址，这个是去除参数后的地址，如：`/pages/user/info`
	 */
	path?: PagesJson.PagePath
	/**
	 * 当前路由的 `query` 传参
	 */
	query?: UTSJSONObject
	/**
	 * 路由隐式传参，替代旧版本的 `params`
	 */
	data?: UTSJSONObject
	/**
	 * 被注册的路由重定向守卫
	 */
	redirect?: RouteRecordRedirectOption
	/**
	 * 是否开启启动拦截，开启后 `beforeEnter` 和 `redirect` 会在实例加载成功后就立刻挂载全局，适用于应用启动拦截的场景，如首页未登录直接跳转登录页等。
	 */
	startupIntercept?: boolean
	/**
	 * 窗口显示的动画类型，`v1.1.0` 及以上版本支持
	 * + auto: 自动选择动画效果
	 * + none: 无动画效果
	 * + slide-in-right: 从右侧横向滑动效果
	 * + slide-in-left: 左侧横向滑动效果
	 * + slide-in-top: 从上侧竖向滑动效果
	 * + slide-in-bottom: 从下侧竖向滑动效果
	 * + fade-in: 从透明到不透明逐渐显示效果
	 * + zoom-out: 从小到大逐渐放大显示效果
	 * + zoom-fade-out: 从小到大逐渐放大并且从透明到不透明逐渐显示效果
	 * + pop-in: 从右侧平移入栈动画效果
	 */
	animationType?: string
	/**
	 * 窗口显示动画的持续时间，单位为 ms
	 * + `v1.1.0` 及以上版本支持
	 */
	animationDuration?: number
}
```

<a id="api_types_RouterOptions"></a>
#### RouterOptions
+ `v1.1.0` 及以上版本

	```
	/**
	 * 创建路由器的配置参数
	 */
	export type RouterOptions = {
		/**
		 * 创建路由器时的初始化路由表
		 */
		routes: RouteRecordNormalized[]
	}
	```
+ `v1.1.0` 以下版本
	
	```uts
	export type RouterOptions = {
		routes: RouteRecordNormalized[];
		parseQuery?: (url: string) => UTSJSONObject;
		stringifyQuery?: (query: UTSJSONObject) => string;
		debug?: boolean;
	};
	```

<a id="api_types_NavigationFailureType"></a>
#### NavigationFailureType
```
/**
 * 导航失败类型
 * + aborted 被中止，比如路由守卫中手动返回了 `false`
 * + cancelled 被取消，比如下一个导航已经开始而导航失败
 * + duplicated 重复导航，比如现在已经处于要导航到的路径而导航失败
 */
export type NavigationFailureType = 
| 'aborted'
| 'cancelled'
| 'duplicated'
```
> **提示**
> 
> `v1.1.0` 及以上版本支持

<a id="api_types_RouteLocationRaw"></a>
#### RouteLocationRaw
```uts
export type RouteLocationRaw = {
	query: any;
};
```

<a id="api_types_NavigationFailure"></a>
#### NavigationFailure
```
/**
 * 导航失败信息
 * + from 当前路由信息
 * + to 即将前往的路由信息
 * + type 导航失败类型
 */
export type NavigationFailure = {
	from: RouteLocationNormalizedLoaded
	to: RouteLocationNormalizedLoaded,
	type?: NavigationFailureType
}
```
> **提示**
> 
> `v1.1.0` 及以上版本支持

<a id="api_types_NavigationHookAfter"></a>
#### NavigationHookAfter
+ `v1.1.0` 及以上版本

	```
	export type NavigationHookAfter = (to: RouteLocationNormalizedLoaded, from: RouteLocationNormalizedLoaded, failure: NavigationFailure | null) => any
	```
+ `v1.1.0` 以下版本

	```uts
	export type NavigationHookAfter = (to: RouteLocationNormalizedLoaded, from: RouteLocationNormalizedLoaded) => void;
	```
	
<a id="api_types_ErrorListener"></a>
#### ErrorListener
```
/**
 * `onError` 回调定义
 */
export type ErrorListener = (error: any) => void
```
> **提示**
> 
> `v1.1.0` 及以上版本支持

#### Page
该类型定义是因为 `v1.0.2` 版本前 `getCurrentPages` 返回的 `Page` 类型无法直接在web使用，暂时只能自己定义类型规避web上的问题

```uts
export type Page = {
	route: string;
	options: Map<string, string | null>
};
```
> **说明**
> 
> `v1.0.2` 及以上版本该类型已废弃，使用官方的 `Page`

#### 相关类和函数签名定义

```
/**
 * 路由实例管理
 *  `v1.1.0` 及以上版本已废弃，由interface替代
 */
export declare class KuxRouter {
	/**
	 * 当前路由表信息
	 */
	public _routes: RouteLocationNormalizedLoaded[];
	/**
	 * 即将前往的路由信息
	 */
	public _to: RouteLocationNormalizedLoaded;
	
	constructor();
	
	/**
	 * 当前路由信息
	 * @returns {RouteLocationNormalizedLoaded}
	 */
	public currentRoute(): RouteLocationNormalizedLoaded;
	
	/**
	 * 路由跳转后的守卫
	 * @description 路由跳转后执行的导航守卫钩子，全局都会生效
	 * @param {RouteLocationNormalizedLoaded} to 即将访问的路由信息
	 * @param {RouteLocationNormalizedLoaded} from 即将离开的路由信息
	 */
	public afterEach(callback: (to: RouteLocationNormalizedLoaded, from: RouteLocationNormalizedLoaded) => void): void;
	
	/**
	 * 路由跳转前的守卫
	 * @description 路由跳转前执行的导航守卫钩子，全局都会生效
	 * @param {RouteLocationNormalizedLoaded} to 即将访问的路由信息
	 * @param {RouteLocationNormalizedLoaded} from 即将离开的路由信息
	 */
	public beforeEach(callback: (to: RouteLocationNormalizedLoaded, from: RouteLocationNormalizedLoaded) => Promise<NavigationGuardReturn>): void;
	
	/**
	 * 移除全局的 `beforeEach` 路由跳转前的守卫
	 */
	public removeBeforeEach(): void;
	
	/**
	 * 动态增加routes配置中的路由信息
	 * @param {RouteRecordNormalized} routeRecord 增加的路由信息
	 */
	public addRoute(routeRecord: RouteRecordNormalized): void;
	
	/**
	 * 动态更新routes配置中的路由信息
	 * @param {RouteRecordNormalized} routeRecord 增加的路由信息
	 */
	public updateRoute(routeRecord: RouteRecordNormalized): void;
	
	/**
	 * 判断路由是否存在
	 * @description 根据getCurrentPages返回的当前路由栈信息判断，和routes中的无关
	 * @param {string} path 页面地址
	 * @returns {boolean}
	 */
	public hasRoute(path: string): boolean;
	
	/**
	 * 获取所有的路由信息
	 * @description 根据 `getCurrentPages` 返回的当前路由栈信息判断，和routes的信息无关
	 * @returns {RouteLocationNormalizedLoaded[]}
	 */
	public getRoutes(): RouteLocationNormalizedLoaded[];
	
	/**
	 * 关闭当前页面，返回上一页面或多级页面
	 * @param {number} delta 返回的页面数，如果 `delta` 大于现有页面数，则返回到首页
	 */
	public back(delta?: number): void;
	
	/**
	 * 获取即将离开的路由信息
	 * @returns {RouteLocationNormalizedLoaded}
	 */
	public from(): RouteLocationNormalizedLoaded;
	
	/**
	 * 跳转到新的路由页面
	 * @param {string} to 页面地址
	 * @param {RouteLocationRaw} options 可选配置项
	 */
	public push(to: string, options?: RouteLocationRaw): Promise<void>;
}

/**
 * 定义一个路由器实例接口
 *  `v1.1.0` 及以上版本支持
 */
export interface IRouter {
	/**
	 * 当前的 `RouteLocationNormalized`
	 */
	// readonly currentRoute: Ref<RouteLocationNormalizedLoaded | null>
	
	/**
	 * 创建路由器时的原始选项对象。
	 * + `v1.1.0` 及以上版本支持
	 */
	readonly options: RouterOptions | null
	
	/**
	 * 获取即将离开的路由信息。
	 * + `v1.1.0` 及以上版本支持
	 */
	from: RouteLocationNormalizedLoaded | null
	
	/**
	 * 动态增加 routes 配置中的路由信息。
	 * @param { RouteRecordNormalized } route `routes` 中的路由配置信息。
	 * @deprecated 该方法即将被废弃，由路由库提供新的自动化解决方案替代。
	 */
	addRoute (route: RouteRecordNormalized): Promise<any | null>
	
	/**
	 * 动态更新 `routes` 配置中的路由信息。
	 * @param { RouteRecordNormalized } route `routes` 中的路由配置信息。
	 */
	updateRoute (route: RouteRecordNormalized): Promise<any | null>
	
	/**
	 * 获取当前路由信息
	 * @returns 返回一个规范化的路由信息。
	 */
	currentRoute (): RouteLocationNormalizedLoaded
	
	/**
	 * 添加一个导航钩子，它会在每次导航之后被执行。返回一个用来移除该钩子的函数。
	 * @param { NavigationHookAfter } guard 要加入的导航钩子
	 */
	afterEach (guard: NavigationHookAfter): () => void
	
	/**
	 * 用来移除所有的 `afterEach` 钩子函数
	 */
	removeAfterEach (): void
	
	/**
	 * 通过调用 uni.navigateBack() 在可能的情况下在历史中后退。相当于 router.go(-1)。
	 * @param { number } delta 回退的页面数，如果 delta 大于现有页面数，则返回到首页
	 */
	back (delta?: number): void
	
	/**
	 * 通过调用 uni.navigateBack() 在可能的情况下在历史中后退。相当于 router.go(-1)。
	 * @param { number } delta 回退的页面数，如果 delta 大于现有页面数，则返回到首页
	 * @param { RouteLocationNormalizedOptions } options 路由跳转时的配置参数，比如跳转时的转场动画配置
	 */
	back (delta?: number, options?: RouteLocationNormalizedOptions): void
	
	/**
	 * 添加一个导航钩子，它会在每次导航之前被执行。返回一个用来移除该钩子的函数。
	 * @param { NavigationGuardWithThis } guard 要加入的导航钩子
	 */
	beforeEach (guard: NavigationGuardWithThis): () => void
	
	/**
	 * 用来移除所有的 `beforeEach` 守卫。
	 * + `v1.1.0` 及以上版本支持。
	 */
	removeBeforeEach (): void
	
	/**
	 * 添加一个导航守卫，它会在导航将要被解析之前被执行。此时所有组件都已经获取完毕，且其它导航守卫也都已经完成调用。返回一个用来移除该守卫的函数。
	 * @param { NavigationGuardWithThis } guard 要加入的导航钩子
	 */
	// beforeResolve (guard: NavigationGuardWithThis): () => void
	
	/**
	 * 通过调用 history.forward() 在可能的情况下在历史中前进。相当于 router.go(1)。
	 */
	// forward (): void
	
	/**
	 * 获得所有路由记录的完整列表。
	 * @returns { RouteRecordNormalized[] } 返回所有路由记录的完整列表。
	 */
	getRoutes (): RouteRecordNormalized[]
	
	// go (delta: number): void
	
	/**
	 * 检查一个给定名称的路由是否存在。
	 * @param { string } name 要检查的路由名称
	 */
	hasRoute (name: string): boolean
	
	/**
	 * 添加一个错误处理器，它会在每次导航遇到未被捕获的错误出现时被调用。其中包括同步和异步被抛出的错误、在任何导航守卫中返回或传入 next 的错误、尝试解析一个需要渲染路由的异步组件时发生的错误。
	 * + `v1.1.0` 及以上版本支持。
	 * @param { ErrorListener } handler 要注册的错误处理器
	 */
	onError (handler: ErrorListener): void
	
	/**
	 * 程序式地通过将一条记录加入到历史栈中来导航到一个新的 URL。
	 * @param { RouteRecordPath } to 要导航到的路由地址
	 */
	push (to: RouteRecordPath): Promise<NavigationFailure | null>
	
	/**
	 * 程序式地通过将一条记录加入到历史栈中来导航到一个新的 URL。
	 * @param { RouteRecordNormalized } to 要导航到的路由信息
	 */
	push (to: RouteRecordNormalized): Promise<NavigationFailure | null>
	
	/**
	 * 程序式地通过将一条记录加入到历史栈中来导航到一个新的 URL。
	 * @param { any } to 要导航到的路由信息
	 * @param { RouteLocationNormalizedOptions } options 路由跳转时的配置参数，比如跳转时的转场动画配置
	 */
	push (to: any): Promise<NavigationFailure | null>
	
	// removeRoute (name: RouteRecordName): void
	
	/**
	 * 程序式地通过替换历史栈中的当前记录来导航到一个新的 URL。
	 * + `v1.1.0` 及以上版本支持。
	 * @param { RouteRecordPath } to 要导航到的路由地址
	 */
	replace (to: RouteRecordPath): Promise<NavigationFailure | null>
	
	/**
	 * 程序式地通过替换历史栈中的当前记录来导航到一个新的 URL。
	 * + `v1.1.0` 及以上版本支持。
	 * @param { RouteRecordNormalized } to 要导航到的路由信息
	 */
	replace (to: RouteRecordNormalized): Promise<NavigationFailure | null>
	
	/**
	 * 程序式地通过替换历史栈中的当前记录来导航到一个新的 URL。
	 * + `v1.1.0` 及以上版本支持。
	 * @param { any } to 要导航到的路由信息
	 */
	replace (to: any): Promise<NavigationFailure | null>
	
	/**
	 * 关闭所有页面，打开到应用内的某个页面
	 * `v1.1.0` 及以上版本支持
	 * @param { RouteRecordPath } to 要导航到的路由地址
	 */
	reLaunch (to: RouteRecordPath): Promise<NavigationFailure | null>
	
	/**
	 * 关闭所有页面，打开到应用内的某个页面
	 * `v1.1.0` 及以上版本支持
	 * @param { RouteRecordNormalized } to 要导航到的路由信息
	 */
	reLaunch (to: RouteRecordNormalized): Promise<NavigationFailure | null>
	
	/**
	 * 关闭所有页面，打开到应用内的某个页面
	 * `v1.1.0` 及以上版本支持
	 * @param { any } to 要导航到的路由信息
	 */
	reLaunch (to: any): Promise<NavigationFailure | null>
	
	/**
	 * 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
	 * `v1.1.0` 及以上版本支持
	 * @param { RouteRecordPath } to 要导航到的路由地址
	 */
	switchTab (to: RouteRecordPath): Promise<NavigationFailure | null>
	
	/**
	 * 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
	 * `v1.1.0` 及以上版本支持
	 * @param { RouteRecordNormalized } to 要导航到的路由信息
	 */
	switchTab (to: RouteRecordNormalized): Promise<NavigationFailure | null>
	
	/**
	 * 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
	 * @param { any } to 要导航到的路由信息
	 */
	switchTab (to: any): Promise<NavigationFailure | null>
	
	/**
	 * 返回一个路由地址的规范化路由信息。
	 * + `v1.1.0` 及以上版本支持。
	 * @param { RouteRecordName } to 路由地址
	 * @returns { RouteLocationNormalizedLoaded } 返回规范化的路由信息
	 */
	resolve (to: RouteRecordName): RouteLocationNormalizedLoaded | null
}

/**
 * 定义一个管理路由实例接口
 *  `v1.1.0` 及以上版本支持
 */
export interface IRoute {
	/**
	 * 获取当前所有路由信息
	 * @returns 返回当前所有路由的规范化路由信息列表
	 */
	getRoutes (): RouteLocationNormalizedLoaded[];
	
	/**
	 * 获取当前路由信息
	 * @returns 返回当前路由的规范化路由信息
	 */
	current (): RouteLocationNormalizedLoaded;
	
	/**
	 * 根据路由地址判断该路由是否存在
	 * @param { string } path 要判断的路由地址
	 */
	hasRoute (path: string): boolean;
	
	/**
	 * 返回一个路由地址的规范化路由信息。
	 * + `v1.1.0` 及以上版本支持。
	 * @param { RouteRecordName } to 路由地址
	 * @returns { RouteLocationNormalizedLoaded } 返回规范化的路由信息
	 */
	resolve (to: RouteRecordName): RouteLocationNormalizedLoaded | null;
}

/**
 * 返回当前路由器实例
 *  `v1.1.0` 以下版本
 */
export declare function useRouter (): KuxRouter;

// v1.1.0 及以上版本
export declare function useRouter (): IRouter

/**
 * 返回当前路由信息
 */
 // v1.1.0 以下版本
export declare function useRoute (): RouteLocationNormalizedLoaded;

// v1.1.0 及以上版本
export declare function useRoute (): RouteLocationNormalizedLoaded

/**
 * 创建一个路由器实例
 */
 // v1.1.0 以下版本
export declare function createRouter(options?: RouterOptions): KuxRouter;

// v1.1.0 及以上版本
export declare function createRouter (options: RouterOptions): IRouter
```
> **说明**
> 
> `HBuilderX` 版本 4.0 及以上才支持。

---
### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

___
### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手